#include "mainwindow.h"
#include <QApplication>
#include <QDir>

int main(int argc, char *argv[])
{
    std::string applicationPath( argv[ 0 ] );

    std::string::size_type indexPath = applicationPath.find_last_of( "/\\" );
    if ( indexPath != std::string::npos )
    {
       // Если задан путь к исполняемому файлу, то данный каталог становится рабочим
       applicationPath = applicationPath.substr( 0, indexPath );
    }
    else
       applicationPath = ".";

    //восстанавливаем путь
    QString pathToFile = QString ( applicationPath.c_str() );
    pathToFile = QDir( pathToFile ).absolutePath();
    applicationPath = pathToFile.toStdString();

    QApplication a(argc, argv);
    MainWindow w;
    w.setAppPath( applicationPath );
    w.show();

    return a.exec();
}
