#include "GLWidget.h"
#include "common.h"

#include <fstream>
#include <iostream>

#include <QTimer>
#include <QFileDialog>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLShader>

static const char *FProgramTest=
  "uniform sampler2D Ytex;\n"
  "uniform sampler2D Utex,Vtex;\n"
  "void main(void) {\n"
  "  highp float nx,ny,r,g,b,y,u,v;\n"
  "  vec4 txl,ux,vx;"
  "  nx=gl_TexCoord[0].x;\n"
  "  ny=gl_TexCoord[0].y;\n"
  "  y=texture2D(Ytex,vec2(nx,ny)).r;\n"
  "  u=texture2D(Utex,vec2(nx,ny)).r;\n"
  "  v=texture2D(Vtex,vec2(nx,ny)).r;\n"

  "  y=1.164*(y-0.0625);\n"
  "  u=u-0.5;\n"
  "  v=v-0.5;\n"

  "  r=y+1.596*v;\n"
  "  g=y-0.392*u-0.813*v;\n"
  "  b=y+2.017*u;\n"

  "  gl_FragColor=vec4(r,g,b,1.0);\n"
  "}\n";


GLWidget::GLWidget(unsigned char *videoData, long len, QWidget *parent)
    //: QGLWidget(QGLFormat(QGL::SampleBuffers), parent)   //Qt4
    : QOpenGLWidget( parent )                              //Qt5
{
    m_width = 176;
    m_height = 144;
    m_videoBuffer = videoData;
    m_videoLength = len;

    m_rgb = NULL;

    Ytex = Utex = Vtex = NULL;
    m_frame = 0;

    QTimer *timer = new QTimer(this);
    //connect(timer, SIGNAL( timeout() ), this, SLOT( updateGL() ) );  ///Qt4
    connect(timer, SIGNAL( timeout() ), this, SLOT( repaint() ) );     ///Qt5
    timer->start(40);
}

GLWidget::~GLWidget()
{
    glDeleteTextures( 1, &y_tex );
    glDeleteTextures( 1, &u_tex );
    glDeleteTextures( 1, &v_tex );

    QOpenGLFunctions *glFuncs = QOpenGLContext::currentContext()->functions();
    glFuncs->glDeleteProgram( m_program->programId() );

    if( m_videoBuffer )
        delete[] m_videoBuffer;
    if( m_rgb )
        delete m_rgb;

    if( Ytex )
        delete Ytex;
    if( Utex )
        delete Utex;
    if( Vtex )
        delete Vtex;
}


QSize GLWidget::minimumSizeHint() const
{
    return QSize( 300, 300 );
}

QSize GLWidget::sizeHint() const
{
    return QSize( 400, 400 );
}


void GLWidget::initializeGLCPU()
{
    glGenTextures( 1, m_texture );
    glBindTexture( GL_TEXTURE_2D, m_texture[ 0 ] );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL );

    glClearColor( 0,0,0,0 );
}

void GLWidget::initializeGLGPU()
{
    bool isOk = true;
    QOpenGLShader* fshader = new QOpenGLShader( QOpenGLShader::Fragment, this );
    isOk = fshader->compileSourceCode( FProgramTest );
    if( !isOk )
        return;

    m_program = new QOpenGLShaderProgram( this );
    m_program->addShader( fshader );
    m_program->link();

    glGenTextures( 1, &y_tex );
    glBindTexture( GL_TEXTURE_2D, y_tex );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_R8, m_width, m_height, 0, GL_RED, GL_UNSIGNED_BYTE, NULL );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    glGenTextures( 1, &u_tex );
    glBindTexture( GL_TEXTURE_2D, u_tex );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_R8, m_width/2, m_height/2, 0, GL_RED, GL_UNSIGNED_BYTE, NULL );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    glGenTextures( 1, &v_tex );
    glBindTexture( GL_TEXTURE_2D, v_tex );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_R8, m_width/2, m_height/2, 0, GL_RED, GL_UNSIGNED_BYTE, NULL );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    QOpenGLFunctions *glFuncs = QOpenGLContext::currentContext()->functions();
    glFuncs->glUseProgram(m_program->programId());
    glFuncs->glUniform1i( glFuncs->glGetUniformLocation(m_program->programId(), "Ytex" ), 0 );
    glFuncs->glUniform1i( glFuncs->glGetUniformLocation(m_program->programId(), "Utex" ), 1 );
    glFuncs->glUniform1i( glFuncs->glGetUniformLocation(m_program->programId(), "Vtex" ), 2 );
}

void GLWidget::initializeGL()
{
    glEnable( GL_TEXTURE_2D );
    /* //Use for CPU
    initializeGLCPU()
    //*/
    //*  //  FOR GPU
    initializeGLGPU();
    // */

    m_frame = 0;
}

void GLWidget::draw()
{
    int x = 1;
    int y = 1;

    glPushMatrix();
    glBegin( GL_QUADS );

    glTexCoord2i( 0,0 );
    glVertex2i( 0,0 );

    glTexCoord2i( x, 0 );
    glVertex2i( x, 0 );

    glTexCoord2i( x, y );
    glVertex2i( x, y );

    glTexCoord2i( 0, y );
    glVertex2i( 0, y );

    glEnd();

    glPopMatrix();
    glFlush();
}

/*  ///CPU
void GLWidget::paintGL()
{
    long frame_size_y = getImageSize( m_width, m_height );
    long frame_size_uv = getSizeUV( m_width, m_height );
    long frame_size = getYUVFrameSize( frame_size_y, frame_size_uv );

    int frameCount = m_videoLength/frame_size;

    if( !m_rgb )
    {
        try
        {
            m_rgb = new unsigned char[ frame_size ];
        }
        catch( std::exception& )
        {
            return;
        }
    }

    if( frameCount == m_frame )
        m_frame = frameCount - 1;
    else
        memcpy( m_rgb, m_videoBuffer + frame_size * m_frame, frame_size*sizeof( unsigned char ) );

    unsigned char* data =  yuv2rgb( m_width, m_height, m_rgb );

    glBindTexture(GL_TEXTURE_2D, m_texture[0] );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, data );

    glClear( GL_COLOR_BUFFER_BIT );
    glBindTexture( GL_TEXTURE_2D, m_texture[ 0 ] );

    draw();

    delete[] data;
    m_frame++;
}
//*/

//*  ///GPU
void GLWidget::paintGL()
{
    long frame_size_y = getImageSize( m_width, m_height );
    long frame_size_uv = getSizeUV( m_width, m_height );
    long frame_size = getYUVFrameSize( frame_size_y, frame_size_uv );

    int frameCount = m_videoLength/frame_size;
    if( !Ytex )
    {
        try
        {
            Ytex = new GLubyte[ frame_size_y ];
            Utex = new GLubyte[ frame_size_uv ];
            Vtex = new GLubyte[ frame_size_uv ];
        }
        catch( std::exception& )
        {
            if( Ytex )
            {
                delete Ytex;
                Ytex = NULL;
            }
            if( Utex )
            {
                delete Utex;
                Utex = NULL;
            }
            if( Vtex )
            {
                delete Vtex;
                Vtex = NULL;
            }
            return;
        }
    }

    if( frameCount == m_frame )
        m_frame = frameCount - 1;
    else
    {
        memcpy( Ytex, m_videoBuffer + frame_size * m_frame, frame_size_y*sizeof( unsigned char ) );
        memcpy( Utex, m_videoBuffer + frame_size * m_frame + frame_size_y, frame_size_uv*sizeof( unsigned char ) );
        memcpy( Vtex, m_videoBuffer + frame_size * m_frame + frame_size_y + frame_size_uv, frame_size_uv*sizeof( unsigned char ) );
    }

    QOpenGLFunctions *glFuncs = QOpenGLContext::currentContext()->functions();

    glClear( GL_COLOR_BUFFER_BIT );

    setPixels( ( unsigned char* )Ytex, y_tex, m_width, m_height );
    setPixels( ( unsigned char* )Utex, u_tex, m_width/2, m_height/2 );
    setPixels( ( unsigned char* )Vtex, v_tex, m_width/2, m_height/2 );

    glFuncs->glUseProgram(m_program->programId());

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, y_tex);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, u_tex);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, v_tex);

    draw();

    m_frame++;
}
//*/

void GLWidget::resizeGL(int width, int height)
{
    glViewport( 0, 0, width, height );

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho( 0, 1, 1, 0,  0, 1 );
    glMatrixMode( GL_MODELVIEW );
}


void GLWidget::slotOnResetVideo()
{
    m_frame = 0;
}

void GLWidget::slotOnOpenVideo()
{
    QString fName = QFileDialog::getOpenFileName( this, "YUV420 video", QString(),"*.yuv" );
    if( fName.isEmpty() )
        return;

    std::ifstream is;
    is.open ( fName.toStdString().c_str(), std::ios::binary );
    if( !is.is_open() )
    {
        std::cout << "Cannot open video file!" << std::endl;
        return;
    }
    is.seekg( 0, std::ios::end );
    m_videoLength = is.tellg();
    is.seekg( 0, std::ios::beg );
    if( m_videoBuffer )
        delete m_videoBuffer;

    m_videoBuffer = new unsigned char[ m_videoLength + 1 ];
    is.read( (char* )m_videoBuffer, m_videoLength );
    is.close();

    slotOnResetVideo();
}


void GLWidget::setPixels( uint8_t* pixels, int level, int width, int height )
{
    glBindTexture( GL_TEXTURE_2D, level );
    glPixelStorei( GL_UNPACK_ROW_LENGTH, width );
    glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RED, GL_UNSIGNED_BYTE, pixels );
}
