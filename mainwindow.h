#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QLabel;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setAppPath(const std::string &path );
    ~MainWindow();

protected:
    std::string m_path;

protected slots:
    void writeVideo();
    void writeVideoThread();
    void writeVideoSSE();

    void writeVideoRGB();
    void writeVideoRGBThread();
    void writeVideoRGBSSE();

private:
    Ui::MainWindow *ui;

    QLabel* m_lbConvertY;
    QLabel* m_lbConvertYThread;
    QLabel* m_lbConvertYSSE;

    QLabel* m_lbConvertRGB;
    QLabel* m_lbConvertRGBThread;
    QLabel* m_lbConvertRGBSSE;
};

#endif // MAINWINDOW_H
