#ifndef COMMON_H
#define COMMON_H


//!Входной видео файл
//#define TEST_VIDEO "/home/yakut/test.yuv"
#define TEST_VIDEO "test.yuv"

//!Видео из 3-х кадров. Наложение по RGB
//#define TEST_OUTPUT_VIDEO1 "/home/yakut/MyVideo.yuv"
#define TEST_OUTPUT_VIDEO1 "RGBMyVideo.yuv"

//!Видео. Наложение по Y каналу
//#define TEST_OUTPUT_VIDEO2 "/home/yakut/0MyVideo.yuv"
#define TEST_OUTPUT_VIDEO2 "YChanelMyVideo.yuv"

//!Входная картинка
//#define TEST_BMPIMAGE "/home/yakut/00.bmp"
#define TEST_BMPIMAGE "00.bmp"

//!Size
long getImageSize( int width, int height );
//!UV SIZE
long getSizeUV( int width, int height );
//!FrameSize
long getYUVFrameSize( int frame_size_y, int frame_size_uv );

//!Конвертация
unsigned char* yuv2rgb( long width, long height, unsigned char* data );
void yuv2rgb( long width, long height, unsigned char* data, unsigned char* image );
//!Пока проблемы с нормальной реализацией
void yuv2rgbSSE( long width, long height, unsigned char* data, unsigned char* image );

//!Конвертация
unsigned char* rgb2yuv( long width, long height, unsigned char* data );
unsigned char* bgr2yuv( long width, long height, unsigned char* data );
void bgr2yuv( long width, long height, unsigned char* data, unsigned char* yuv );
void rgb2yuv( long width, long height, unsigned char* data, unsigned char* yuv );

//!Вертикальное зеркалирование
void mirrorUpDown( unsigned char* pixelData, int width, int height );
//!Замена R и B цветов
void convertBGR2RGB( unsigned char* buf, int width, int height );


//! Результирующая картинка, по RGB
unsigned char* getRes( unsigned char* pixelData, unsigned char* bmpData, int width, int height);
void getRes( unsigned char* pixelData, unsigned char* bmpData, unsigned char* rgb, int width, int height );

//! Результирующая картинка, по Y
unsigned char* getResYChanel( unsigned char* buffer, unsigned char* bmpData, int width, int height );
//!Подготовка по Y
void frameForWriting( int width, int height, unsigned char* bufTmp, unsigned char* bufferBmp, unsigned char* frameBuf );
void frameForWritingSSE( int width, int height, unsigned char* bufTmp, unsigned char* bufferBmp, unsigned char* frameBuf );

//!Получение BMP изображения
unsigned char* getBmp();

//!Получить видео
unsigned char* getVideoData( long& length, int& width, int& height );

//!Подготовка в потоке файла для записи
void threadWriteY(int left, int right, unsigned char *bufferBmp, unsigned char* bufferVideo, long frame_size, int width, int height, unsigned char* bufferVideoOut );
void threadWriteRGB(int left, int right, unsigned char *bufferBmp, unsigned char* bufferVideo, long frame_size, int width, int height, unsigned char *bufferVideoOut );

#endif // GLWidget_H
