#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "common.h"
#include "GLWidget.h"

#include <QLabel>
#include <QHBoxLayout>
#include <QProcess>
#include <QPushButton>
#include <QTime>

#include <fstream>
#include <iostream>
#include <thread>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    resize( 800, 600 );

    unsigned char * image = NULL;
    unsigned char* bufferRes = NULL;
    unsigned char* bufferResYChanel = NULL;
    unsigned char* bmpImage = NULL;
    unsigned char* bmpImageOrig = NULL;
    unsigned char* bufferVideo = NULL;

    //!Read Video
    int width = 176;
    int height = 144;

    std::ifstream is;
    is.open ( TEST_VIDEO, std::ios::binary );
    if( !is.is_open() )
    {
        fprintf( stderr, "Cannot open video stream" );
        return;
    }
    is.seekg (0, std::ios::end);
    long length = is.tellg();
    is.seekg (0, std::ios::beg);
    bufferVideo = new unsigned char[length + 1];
    is.read( (char* )bufferVideo, length );
    is.close();
    /////

    bmpImageOrig = getBmp();
    int bmpImageSize =  width*height*3;
    bmpImage = new unsigned char[ bmpImageSize ];
    memcpy( bmpImage, bmpImageOrig, bmpImageSize*sizeof(unsigned char) );
    convertBGR2RGB( bmpImage, width, height );
    mirrorUpDown( bmpImage, width, height );

    if( !bmpImage )
    {
        fprintf( stderr, "Cannot open BMP Image\n" );
        return;
    }

    image = yuv2rgb( width, height, bufferVideo );
    bufferRes = getRes( image, bmpImage, width, height );
    unsigned char* yuvBMP = rgb2yuv( width, height, bmpImage );
    bufferResYChanel = getResYChanel( bufferVideo, yuvBMP, width, height );
    delete[] yuvBMP;

    QImage imgBmp( bmpImage, width, height, QImage::Format_RGB888 );
    QImage img( image, width, height, QImage::Format_RGB888 );
    QImage imgRes( bufferRes, width, height, QImage::Format_RGB888 );
    QImage imgResY( bufferResYChanel, width, height, QImage::Format_RGB888 );


    //!INIT GUI
    QWidget* center = new QWidget;
    setCentralWidget( center );

    QHBoxLayout *l = new QHBoxLayout;
    QVBoxLayout *v = new QVBoxLayout;
    QHBoxLayout *lVideo = new QHBoxLayout;
    QHBoxLayout *lCommandButton = new QHBoxLayout;
    QHBoxLayout *lCommandButtonRGB = new QHBoxLayout;

    QLabel* lbBmp = new QLabel( center );
    lbBmp->setToolTip( "Исходная BMP картинка" );
    QLabel* lb = new QLabel( center );
    lb->setToolTip( "Первый кадр видео" );
    QLabel* result = new QLabel( center );
    result->setToolTip( "Наложение по RGB" );
    QLabel* resultY = new QLabel( center );
    resultY->setToolTip( "Наложение по Y" );

    lb->setPixmap( QPixmap::fromImage( img ) );
    lbBmp->setPixmap( QPixmap::fromImage( imgBmp ) );
    result->setPixmap( QPixmap::fromImage( imgRes ) );
    resultY->setPixmap( QPixmap::fromImage( imgResY ) );

    GLWidget* glWgt = new GLWidget( bufferVideo, length, center );

    QPushButton* btResetVideo = new QPushButton( "Reset video", center );
    connect( btResetVideo, SIGNAL( pressed() ), glWgt, SLOT( slotOnResetVideo() ) );

    QPushButton* btOpenVideo = new QPushButton( "Open video", center );
    connect( btOpenVideo, SIGNAL( pressed() ), glWgt, SLOT( slotOnOpenVideo() ) );


    QPushButton* btMakeVideoY = new QPushButton( "Y Chanel Video", center );
    connect( btMakeVideoY, SIGNAL( pressed() ), this, SLOT( writeVideo() ) );

    QPushButton* btMakeVideoThread = new QPushButton( "Y Chanel Video Thread", center );
    connect( btMakeVideoThread, SIGNAL( pressed() ), this, SLOT( writeVideoThread() ) );

    QPushButton* btMakeVideoSSE = new QPushButton( "Y Chanel Video SSE", center );
    connect( btMakeVideoSSE, SIGNAL( pressed() ), this, SLOT( writeVideoSSE() ) );

    m_lbConvertY = new QLabel( center );
    m_lbConvertYThread = new QLabel( center );
    m_lbConvertYSSE = new QLabel( center );


    QPushButton* btMakeVideoRGB = new QPushButton( "RGB Chanel Video", center );
    connect( btMakeVideoRGB, SIGNAL( pressed() ), this, SLOT( writeVideoRGB() ) );

    QPushButton* btMakeVideoRGBThread = new QPushButton( "RGB Chanel Video Thread", center );
    connect( btMakeVideoRGBThread, SIGNAL( pressed() ), this, SLOT( writeVideoRGBThread() ) );

    QPushButton* btMakeVideoRGBSSE = new QPushButton( "RGB Chanel Video SSE", center );
    connect( btMakeVideoRGBSSE, SIGNAL( pressed() ), this, SLOT( writeVideoRGBSSE() ) );

    m_lbConvertRGB = new QLabel( center );
    m_lbConvertRGBThread = new QLabel( center );
    m_lbConvertRGBSSE = new QLabel( center );


    l->addWidget( lbBmp );
    l->addWidget( lb );
    l->addWidget( result );
    l->addWidget( resultY );
    lVideo->addWidget( glWgt );
    lVideo->addWidget( btResetVideo );
    lVideo->addWidget( btOpenVideo );
    lCommandButton->addWidget( btMakeVideoY );
    lCommandButton->addWidget( m_lbConvertY );
    lCommandButton->addWidget( btMakeVideoThread );
    lCommandButton->addWidget( m_lbConvertYThread );
    lCommandButton->addWidget( btMakeVideoSSE );
    lCommandButton->addWidget( m_lbConvertYSSE );
    lCommandButtonRGB->addWidget( btMakeVideoRGB );
    lCommandButtonRGB->addWidget( m_lbConvertRGB );
    lCommandButtonRGB->addWidget( btMakeVideoRGBThread );
    lCommandButtonRGB->addWidget( m_lbConvertRGBThread );
    lCommandButtonRGB->addWidget( btMakeVideoRGBSSE );
    lCommandButtonRGB->addWidget( m_lbConvertRGBSSE );

    v->addLayout( l );
    v->addLayout( lCommandButton );
    v->addLayout( lCommandButtonRGB );
    v->addLayout( lVideo );
    center->setLayout( v );
    setCentralWidget( center );
    /////


    //writeCombineVideo( image, bmpImageOrig, bufferRes, width, height );
    //test( width, height, bmpImage, bufferVideo, length );

    delete[] bufferRes;
    delete[] bufferResYChanel;
    delete[] image;
    delete[] bmpImage;
    delete[] bmpImageOrig;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setAppPath( const std::string& path )
{
    m_path = path;
}


void MainWindow::writeVideo()
{
    QTime t;
    t.start();

    unsigned char* bufferBmp = NULL;
    unsigned char* bufferVideo = NULL;
    unsigned char* frameBuf = NULL;
    unsigned char* bufTmp = NULL;

    int width, height;
    long length;

    bufferVideo = getVideoData( length, width, height );

    long frame_size_y = getImageSize( width, height );
    long frame_size_uv = getSizeUV( width, height );
    long frame_size = getYUVFrameSize( frame_size_y, frame_size_uv );
    int frameCount = length/frame_size;

    unsigned char* bmpImageOrig = NULL;
    bmpImageOrig = getBmp();
    bufferBmp = bgr2yuv( width, height, bmpImageOrig );
    delete[] bmpImageOrig;

    try
    {
        frameBuf = new unsigned char[ frame_size ];
        bufTmp = new unsigned char[ frame_size ];
    }
    catch( std::exception& )
    {
        if( frameBuf )
            delete[] frameBuf;
        if( bufTmp )
            delete[] bufTmp;

        std::cout << "Allocation buffer memory error!" << std::endl;
        return;
    }

    FILE *videoptr;
    videoptr = fopen( TEST_OUTPUT_VIDEO2, "wb" );
    if( !videoptr )
    {
        std::cout << "Cannot file for writing!" << std::endl;
        return;
    }

    for( int i = 0; i < frameCount; ++i )
    {
        memcpy( bufTmp, bufferVideo + frame_size * i, frame_size*sizeof(unsigned char) );
        frameForWriting( width, height, bufTmp, bufferBmp, frameBuf );
        fwrite( frameBuf , sizeof( unsigned char), frame_size, videoptr );
    }

    fclose( videoptr );

    delete[] bufTmp;
    delete[] frameBuf;
    delete[] bufferBmp;

    m_lbConvertY->setText( QString::number( t.elapsed() ) );
}

void MainWindow::writeVideoThread()
{
    size_t threadCount = std::thread::hardware_concurrency();
    if( threadCount < 1 )
        threadCount = 1;

    std::cout << "Thread Count: " << threadCount << std::endl;

    QTime t;
    t.start();

    unsigned char* bufferBmp = NULL;
    unsigned char* bufferVideo = NULL;
    unsigned char* bufferVideoOut = NULL;

    int width, height;
    long length;


    bufferVideo = getVideoData( length, width, height );
    if( !bufferVideo )
        return;

    try
    {
        bufferVideoOut = new unsigned char[ length ];
    }
    catch( std::exception& )
    {
        return;
    }

    long frame_size_y = getImageSize( width, height );
    long frame_size_uv = getSizeUV( width, height );
    long frame_size = getYUVFrameSize( frame_size_y, frame_size_uv );
    int frameCount = length/frame_size;

    unsigned char* bmpImageOrig = NULL;
    bmpImageOrig = getBmp();
    //!Надо перевернуть
    mirrorUpDown( bmpImageOrig, width, height );
    bufferBmp = bgr2yuv( width, height, bmpImageOrig );

    delete[] bmpImageOrig;

    FILE *videoptr;
    videoptr = fopen( TEST_OUTPUT_VIDEO2, "wb" );
    if( !videoptr )
    {
        std::cout << "Cannot file for writing!" << std::endl;
        return;
    }

    std::vector< std::thread* > threadsPull;
    threadsPull.resize( threadCount );
    int step = frameCount/threadCount;

    for( size_t i = 0; i < threadCount; ++i )
    {
        int left = i*step;
        threadsPull[ i ]  = new std::thread( threadWriteY, left, left+step, bufferBmp, bufferVideo, frame_size, width, height, bufferVideoOut );
    }

    for( size_t i = 0; i < threadCount; ++i )
        threadsPull[ i ]->join();

    fwrite( bufferVideoOut , sizeof( unsigned char), length, videoptr );

    fclose( videoptr );

    delete[] bufferVideoOut;
    delete[] bufferVideo;
    delete[] bufferBmp;
    for( size_t i = 0; i < threadCount; ++i )
        delete threadsPull[ i ];

    m_lbConvertYThread->setText( QString::number( t.elapsed() ) );
}

void MainWindow::writeVideoSSE()
{
    QTime t;
    t.start();

    unsigned char* bufferBmp = NULL;
    unsigned char* bufferVideo = NULL;
    unsigned char* frameBuf = NULL;
    unsigned char* bufTmp = NULL;

    int width, height;
    long length;

    bufferVideo = getVideoData( length, width, height );

    long frame_size_y = getImageSize( width, height );
    long frame_size_uv = getSizeUV( width, height );
    long frame_size = getYUVFrameSize( frame_size_y, frame_size_uv );
    int frameCount = length/frame_size;

    unsigned char* bmpImageOrig = NULL;
    bmpImageOrig = getBmp();
    bufferBmp = bgr2yuv( width, height, bmpImageOrig );
    delete[] bmpImageOrig;

    try
    {
        frameBuf = new unsigned char[ frame_size ];
        bufTmp = new unsigned char[ frame_size ];
    }
    catch( std::exception& )
    {
        if( frameBuf )
            delete[] frameBuf;
        if( bufTmp )
            delete[] bufTmp;

        std::cout << "Allocation buffer memory error!" << std::endl;
        return;
    }

    FILE *videoptr;
    videoptr = fopen( TEST_OUTPUT_VIDEO2, "wb" );
    if( !videoptr )
    {
        std::cout << "Cannot file for writing!" << std::endl;
        return;
    }

    for( int i = 0; i < frameCount; ++i )
    {
        memcpy( bufTmp, bufferVideo + frame_size * i, frame_size*sizeof(unsigned char) );
        frameForWritingSSE( width, height, bufTmp, bufferBmp, frameBuf );
        fwrite( frameBuf , sizeof( unsigned char), frame_size, videoptr );
    }

    fclose( videoptr );

    delete[] bufTmp;
    delete[] frameBuf;
    delete[] bufferBmp;

    m_lbConvertYSSE->setText( QString::number( t.elapsed() ) );
}


void MainWindow::writeVideoRGB()
{
    QTime t;
    t.start();

    unsigned char* bufferBmp = NULL;
    unsigned char* bufferVideo = NULL;
    unsigned char* frameBuf = NULL;
    unsigned char* bufTmp = NULL;
    unsigned char* bufRGBTmp = NULL;
    unsigned char* tmpBufRGB = NULL;

    int width, height;
    long length;

    bufferVideo = getVideoData( length, width, height );
    if( !bufferVideo )
        return;

    int size = width*height*3;

    long frame_size_y = getImageSize( width, height );
    long frame_size_uv = getSizeUV( width, height );
    long frame_size = getYUVFrameSize( frame_size_y, frame_size_uv );
    int frameCount = length/frame_size;

    bufferBmp = getBmp();
    convertBGR2RGB( bufferBmp, width, height );
    mirrorUpDown( bufferBmp, width, height );

    try
    {
        frameBuf = new unsigned char[ frame_size ];
        bufTmp = new unsigned char[ frame_size ];
        bufRGBTmp = new unsigned char[ size ];
        tmpBufRGB = new unsigned char[ size ];
    }
    catch( std::exception& )
    {
        if( frameBuf )
            delete[] frameBuf;
        if( bufTmp )
            delete[] bufTmp;
        if( bufRGBTmp )
            delete[] bufRGBTmp;
        if( tmpBufRGB )
            delete[] tmpBufRGB;

        std::cout << "Allocation buffer memory error!" << std::endl;
        return;
    }

    FILE *videoptr;
    videoptr = fopen( TEST_OUTPUT_VIDEO1, "wb" );
    if( !videoptr )
    {
        std::cout << "Cannot file for writing!" << std::endl;
        return;
    }

    for( int i = 0; i < frameCount; ++i )
    {
        memcpy( bufTmp, bufferVideo + frame_size * i, frame_size*sizeof(unsigned char) );
        yuv2rgb( width, height, bufTmp, bufRGBTmp );
        getRes( bufRGBTmp, bufferBmp, tmpBufRGB, width, height );
        rgb2yuv( width, height, tmpBufRGB, frameBuf );

        fwrite( frameBuf, sizeof( unsigned char), frame_size, videoptr );
    }

    fclose( videoptr );

    delete[] bufTmp;
    delete[] frameBuf;
    delete[] bufferBmp;
    delete[] bufRGBTmp;

    m_lbConvertRGB->setText( QString::number( t.elapsed() ) );
}

void MainWindow::writeVideoRGBThread()
{
    size_t threadCount = std::thread::hardware_concurrency();
    if( threadCount < 1 )
        threadCount = 1;

    std::cout << "Thread Count: " << threadCount << std::endl;

    QTime t;
    t.start();

    unsigned char* bufferBmp = NULL;
    unsigned char* bufferVideo = NULL;
    unsigned char* bufferVideoOut = NULL;
    unsigned char* frameBuf = NULL;
    unsigned char* bufTmp = NULL;

    int width, height;
    long length;

    bufferVideo = getVideoData( length, width, height );
    if( !bufferVideo )
        return;

    try
    {
        bufferVideoOut = new unsigned char[ length ];
    }
    catch( std::exception& )
    {
        return;
    }

    long frame_size_y = getImageSize( width, height );
    long frame_size_uv = getSizeUV( width, height );
    long frame_size = getYUVFrameSize( frame_size_y, frame_size_uv );
    int frameCount = length/frame_size;

    bufferBmp = getBmp();
    convertBGR2RGB( bufferBmp, width, height );
    mirrorUpDown( bufferBmp, width, height );



    FILE *videoptr;
    videoptr = fopen( TEST_OUTPUT_VIDEO1, "wb" );
    if( !videoptr )
    {
        std::cout << "Cannot file for writing!" << std::endl;
        return;
    }

    std::vector< std::thread* > threadsPull;
    threadsPull.resize( threadCount );
    int step = frameCount/threadCount;

    for( size_t i = 0; i < threadCount; ++i )
    {
        int left = i*step;
        threadsPull[ i ]  = new std::thread( threadWriteRGB, left, left+step, bufferBmp, bufferVideo, frame_size, width, height, bufferVideoOut );
    }

    for( size_t i = 0; i < threadCount; ++i )
        threadsPull[ i ]->join();

    fwrite( bufferVideoOut , sizeof( unsigned char), length, videoptr );

    fclose( videoptr );
    delete[] bufTmp;
    delete[] frameBuf;
    delete[] bufferVideoOut;
    delete[] bufferVideo;
    delete[] bufferBmp;
    for( size_t i = 0; i < threadCount; ++i )
        delete threadsPull[ i ];

    m_lbConvertRGBThread->setText( QString::number( t.elapsed() ) );
}

void MainWindow::writeVideoRGBSSE()
{
    QTime t;
    t.start();

    unsigned char* bufferBmp = NULL;
    unsigned char* bufferVideo = NULL;
    unsigned char* frameBuf = NULL;
    unsigned char* bufTmp = NULL;
    unsigned char* bufRGBTmp = NULL;
    unsigned char* tmpBufRGB = NULL;

    int width, height;
    long length;

    bufferVideo = getVideoData( length, width, height );
    if( !bufferVideo )
        return;

    int size = width*height*3;

    long frame_size_y = getImageSize( width, height );
    long frame_size_uv = getSizeUV( width, height );
    long frame_size = getYUVFrameSize( frame_size_y, frame_size_uv );
    int frameCount = length/frame_size;

    bufferBmp = getBmp();
    convertBGR2RGB( bufferBmp, width, height );
    mirrorUpDown( bufferBmp, width, height );

    try
    {
        frameBuf = new unsigned char[ frame_size ];
        bufTmp = new unsigned char[ frame_size ];
        bufRGBTmp = new unsigned char[ size ];
        tmpBufRGB = new unsigned char[ size ];
    }
    catch( std::exception& )
    {
        if( frameBuf )
            delete[] frameBuf;
        if( bufTmp )
            delete[] bufTmp;
        if( bufRGBTmp )
            delete[] bufRGBTmp;
        if( tmpBufRGB )
            delete[] tmpBufRGB;

        std::cout << "Allocation buffer memory error!" << std::endl;
        return;
    }

    FILE *videoptr;
    videoptr = fopen( TEST_OUTPUT_VIDEO1, "wb" );
    if( !videoptr )
    {
        std::cout << "Cannot file for writing!" << std::endl;
        return;
    }

    for( int i = 0; i < frameCount; ++i )
    {
        memcpy( bufTmp, bufferVideo + frame_size * i, frame_size*sizeof(unsigned char) );
        yuv2rgbSSE( width, height, bufTmp, bufRGBTmp );
        getRes( bufRGBTmp, bufferBmp, tmpBufRGB, width, height );
        rgb2yuv( width, height, tmpBufRGB, frameBuf );

        fwrite( frameBuf, sizeof( unsigned char), frame_size, videoptr );
    }

    fclose( videoptr );

    delete[] bufTmp;
    delete[] frameBuf;
    delete[] bufferBmp;
    delete[] bufRGBTmp;

    m_lbConvertRGBSSE->setText( QString::number( t.elapsed() ) );
}
