#ifndef GLWidget_H
#define GLWidget_H

/*  ////Qt4
#include <QtOpenGL/QGLWidget>

class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    GLWidget( unsigned char* videoData, long len, QWidget *parent = 0);
    ~GLWidget();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);


    void draw();

    int m_width;
    int m_height;

    unsigned int m_texture[1];
    unsigned char* m_videoBuffer;
    int m_frame;
    int m_startTime;
    long m_videoLength;
    unsigned char* m_rgba;

    bool m_isFileWasOpen;

public slots:
    void slotOnResetVideo();
    void slotOnOpenVideo();
};
*/

#include <QtWidgets/QOpenGLWidget>
class QOpenGLShaderProgram;
class QOpenGLShader;

class GLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    explicit GLWidget( unsigned char* videoData, long len, QWidget *parent = 0);
    ~GLWidget();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

protected:
    void initializeGL();
    void initializeGLCPU();
    void initializeGLGPU();
    void paintGL();
    void resizeGL(int width, int height);


    void draw();

    int m_width;
    int m_height;

    unsigned int m_texture[1];
    unsigned char* m_videoBuffer;
    int m_frame;
    int m_startTime;
    long m_videoLength;
    unsigned char* m_rgb;

    GLubyte *Ytex,*Utex,*Vtex;
    GLuint y_tex,u_tex,v_tex;

    QOpenGLShaderProgram *m_program;


    void setPixels( uint8_t* pixels, int level, int width, int height );

public slots:
    void slotOnResetVideo();
    void slotOnOpenVideo();
};

#endif // GLWidget_H
