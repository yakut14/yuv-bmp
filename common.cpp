#include "common.h"

#include <fstream>
#include <iostream>
#include <string.h>
#include <thread>

#include <emmintrin.h>

#include <QTime>

long getImageSize( int width, int height )
{
    return width * height;
}

long getSizeUV( int width, int height )
{
    return ((width+1)>>1) * ((height+1)>>1);
}

long getYUVFrameSize( int frame_size_y, int frame_size_uv )
{
    return frame_size_y + 2*frame_size_uv;
}


int clip( float var)
{
    return var > 255 ? 255: (var < 0) ? 0:var;
}


unsigned char* yuv2rgb( long width, long height, unsigned char* data )
{
    unsigned char* rgb;
    try
    {
        rgb = new unsigned char[width*height*3];
    }
    catch( std::exception& )
    {
        std::cout << "Allocation buffer memory error!" << std::endl;
        return NULL;
    }

    yuv2rgb( width, height, data, rgb );

    return rgb;
}


void yuv2rgb( long width, long height, unsigned char* data, unsigned char* image )
{
    unsigned char* rgb = image;

    int j, i;

    int step = width;
    int size = width * height;

    int halfStep = step*0.5;
    int quSize = size*1.25;
    int offset;
    int Y, U, V;
    int R, G, B;
    unsigned char* ptrY = data - step; //-step чтобы нормально пройтись по for
    unsigned char* ptrU = data + size;
    unsigned char* ptrV = data + quSize;

    int uvStep;

    for( i = 0 ; i < height ; ++i )
    {
        ptrY += step;

        for( j = 0 ; j < width ; ++j )
        {
            offset = j*0.5;

            /*  //Default  http://www.equasys.de/colorconversion.html    //YPbPr - RGB Color Format Conversion   SDTV
            Y = ptrY[ j ];
            U = ptrU[ offset ] - 128;
            V = ptrV[ offset ] - 128;

            R = clip( Y + 1.402 * V );
            G = clip( Y - 0.344 * U - 0.714 * V );
            B = clip( Y + 1.772 * U );
            ///////  */

            //*
            //! BT.601
            /*
            Y = ptrY[ j ] - 16;
            U = ptrU[ offset ] - 128;
            V = ptrV[ offset ] - 128;

            Y = 1.164*Y;

            R = clip( Y + 1.596 * V );
            G = clip( Y - 0.392 * U - 0.813 * V );
            B = clip( Y + 2.017 * U );
            */

            Y = ptrY[ j ];
            U = ptrU[ offset ];
            V = ptrV[ offset ];

            R = clip( (1192*(Y-16) + 1634 * (V-128))>>10 );
            G = clip( (1192*(Y-16) - 400 * (U-128) - 833 * (V-128))>>10 );
            B = clip( (1192*(Y-16) + 2066 * (U-128 ) )>>10 );
            //////  */

            (*rgb) = R; rgb++;
            (*rgb) = G; rgb++;
            (*rgb) = B; rgb++;
        }

        uvStep =  i%2 * halfStep;
        ptrU += uvStep;
        ptrV += uvStep;
    }
}


void yuv2rgbSSE( long width, long height, unsigned char* data, unsigned char* image )
{
    unsigned char* rgb = image;

    int j, i;

    int step = width;
    int size = width * height;

    int halfStep = step*0.5;
    int quSize = size*1.25;
    int offset;
    int Y, U, V;
    int R, G, B;
    unsigned char* ptrY = data - step; //-step чтобы нормально пройтись по for
    unsigned char* ptrU = data + size;
    unsigned char* ptrV = data + quSize;

    int uvStep;

    for( i = 0 ; i < height ; ++i )
    {
        ptrY += step;

        for( j = 0 ; j < width ; ++j )
        {
            offset = j*0.5;

            //! BT.601
            //*
            Y = 1.164*(ptrY[ j ] - 16);
            U = ptrU[ offset ] - 128;
            V = ptrV[ offset ] - 128;

            //Y = 1.164*Y;

            R = clip( Y + 1.596 * V );
            G = clip( Y - 0.392 * U - 0.813 * V );
            B = clip( Y + 2.017 * U );
            //*/

            Y = ptrY[ j ];
            U = ptrU[ offset ];
            V = ptrV[ offset ];

            R = (1192*(Y-16) + 1596 * (V-128))>>10;
            G = (1192*(Y-16) - 392 * (U-128) - 813 * (V-128))>>10;
            B = (1192*(Y-16) + 2017 * (U-128 ) )>>10;
            //////  */

            (*rgb) = R; rgb++;
            (*rgb) = G; rgb++;
            (*rgb) = B; rgb++;
        }

        uvStep =  i%2 * halfStep;
        ptrU += uvStep;
        ptrV += uvStep;
    }
}

unsigned char* rgb2yuv( long width, long height, unsigned char* data )
{
    int len = width*height*1.5;
    unsigned char* yuv;
    try
    {
        yuv = new unsigned char[ len ];
    }
    catch( std::exception& )
    {
        std::cout << "Allocation buffer memory error!" << std::endl;
        return NULL;
    }

    rgb2yuv( width, height, data, yuv );

    return yuv;
}


unsigned char* bgr2yuv( long width, long height, unsigned char* data )
{
    int len = width*height*1.5;
    unsigned char* yuv;
    try
    {
        yuv = new unsigned char[ len ];
    }
    catch( std::exception& )
    {
        std::cout << "Allocation buffer memory error!" << std::endl;
        return NULL;
    }

    bgr2yuv( width, height, data, yuv );

    return yuv;
}


void bgr2yuv( long width, long height, unsigned char* data, unsigned char* yuv )
{
    int step = width;
    int size = width * height;
    for( int i = 0 ; i < height ; ++i )
    {
        for( int j = 0 ; j < width ; ++j )
        {
            int offset = ( i*width + j ) * 3;
            int R = data[ offset+2 ];
            int G = data[ offset+1 ];
            int B = data[ offset ];

            int Y = (int)( R *  0.257 + G *  0.504 + B *  0.098 ) + 16;
            int U = (int)( R *  -0.148 + G * -0.291 + B * 0.439 ) + 128;
            int V = (int)( R *  0.439 + G * -0.368 + B *  -0.071 ) + 128;

            int ypos = i * step + j;
            int upos = (i/2) * (step/2) + j/2 + size;
            int vpos = (i/2) * (step/2) + j/2 + 1.25*size;

            yuv[ ypos ] = Y;
            yuv[ upos ] = U;
            yuv[ vpos ] = V;
        }
    }
}

void rgb2yuv( long width, long height, unsigned char* data, unsigned char* yuv )
{
    int step = width;
    int size = width * height;
    for( int i = 0 ; i < height ; ++i )
    {
        for( int j = 0 ; j < width ; ++j )
        {
            int offset = ( i*width + j ) * 3;
            int R = data[ offset ];
            int G = data[ offset+1 ];
            int B = data[ offset+2 ];

            int Y = (int)( R *  0.257 + G *  0.504 + B *  0.098 ) + 16;
            int U = (int)( R *  -0.148 + G * -0.291 + B * 0.439 ) + 128;
            int V = (int)( R *  0.439 + G * -0.368 + B *  -0.071 ) + 128;

            int ypos = i * step + j;
            int upos = (i/2) * (step/2) + j/2 + size;
            int vpos = (i/2) * (step/2) + j/2 + 1.25*size;

            yuv[ ypos ] = Y;
            yuv[ upos ] = U;
            yuv[ vpos ] = V;
        }
    }
}

void mirrorUpDown( unsigned char* pixelData, int width, int height )
{
    width *= 3;
    unsigned char* bufTmp;
    try
    {
        bufTmp = new unsigned char[ width ];
    }
    catch( std::exception& )
    {
        std::cout << "Allocation buffer memory error!" << std::endl;
        return;
    }
    int size = width*height ;
    for( int i = 0; i < height/2; ++i )
    {
        int offset = i*width;
        int offset2 = size - offset - width; //Без -width в первую строку записывается 0 символ

        //!Extra check
        if( offset2 <= 0 )
            break;

        memcpy( bufTmp, pixelData + offset, width*sizeof(unsigned char));
        memcpy( pixelData + offset, pixelData + offset2, width*sizeof(unsigned char));
        memcpy( pixelData + offset2, bufTmp, width*sizeof(unsigned char));
    }

    delete[] bufTmp;
}

//! Результирующая картинка, по RGB
unsigned char* getRes( unsigned char* pixelData, unsigned char* bmpData, int width, int height)
{
    width *= 3;
    unsigned char* rgb;
    int size = width*height;
    try
    {
        rgb = new unsigned char[ size ];
    }
    catch( std::exception& )
    {
        std::cout << "Allocation buffer memory error!" << std::endl;
        return NULL;
    }

    double v11, v22;
    for( int i = 0 ; i < size; ++i )
    {
        v11 = pixelData[ i ]/255.0;
        v22 = bmpData[ i ]/255.0;

        rgb[ i ] = 255 * ( v11 + v22*( 1-v11 ) );
    }

    return rgb;
}

void getRes( unsigned char* pixelData, unsigned char* bmpData, unsigned char* rgb, int width, int height )
{
    int size = width*height*3;

    double v11, v22;
    for( int i = 0 ; i < size; ++i )
    {
        v11 = pixelData[ i ]/255.0;
        v22 = bmpData[ i ]/255.0;

        rgb[ i ] = 255 * ( v11 + v22*( 1-v11 ) );
    }
}

void convertBGR2RGB( unsigned char* buf, int width, int height )
{
    width *= 3;
    unsigned char tmp;
    int offset, offset2;

    for( int i = 0 ; i < height ; ++i )
    {
        for( int j = 0 ; j < width ; j+=3 )
        {
            offset = i * width + j;
            offset2 = offset + 2;

            tmp = buf[ offset2 ];
            buf[ offset2 ] = buf[ offset ];
            buf[ offset ] = tmp;
        }
    }
}

unsigned char* getBmp()
{
    FILE *fileptr;
    unsigned char *buf;
    size_t filelen;

    fileptr = fopen( TEST_BMPIMAGE, "rb" );
    if( !fileptr )
    {
        std::cout << "There is no such BMP file!" << std::endl;
        return NULL;
    }

    fseek( fileptr, 0, SEEK_END );
    int endPos = ftell( fileptr );
    if( endPos < 54 )
    {
        std::cout << "Error in file format" << std::endl;
        fclose( fileptr );
        return NULL;
    }

    rewind (fileptr);

    unsigned char bmpHeaderData[ 54 ];
    size_t ret = fread( &bmpHeaderData, sizeof( unsigned char ), 54, fileptr );
    if( ret != 54 )
    {
        std::cout << "Read Error BMP" << std::endl;
        fclose( fileptr );
        return NULL;
    }

    int w = int( bmpHeaderData[ 18 ] );
    int h = int( bmpHeaderData[ 22 ] );
    int size = w*h;
    filelen = size*3;
    int pos = endPos - filelen;

    try
    {
        buf = new  unsigned char[ filelen ];
    }
    catch( std::exception& )
    {
        std::cout << "Allocation buffer memory error!" << std::endl;
        fclose( fileptr );
        return NULL;
    }

    fseek( fileptr, pos, SEEK_SET );
    ret = fread( buf, sizeof( unsigned char ), filelen, fileptr );
    if( ret != filelen )
    {
        std::cout << "Read Error BMP" << std::endl;
        fclose( fileptr );
        return NULL;
    }


    //convertBGR2RGB( buf, w, h );
    //mirrorUpDown( buf, w, h );

    //unsigned char *istart = buf, *iend = buf + filelen;
    //std::reverse( istart, iend );

    fclose(fileptr);

    return buf;
}

//! Подготовка кадра по Y
void frameForWriting( int width, int height, unsigned char* bufTmp, unsigned char* bufferBmp, unsigned char* frameBuf )
{
    int step = width;
    int size = width * height;
    int quSize = size*1.25;
    int halfStep = step*0.5;
    int offsetY, offsetUV;

    double v11, v22;
    int halfJ;
    int ypos;
    int upos;
    int vpos;

    int resY;

    for( int i = 0 ; i < height; ++i )
    {
        offsetY = i * step;
        offsetUV = int( i * 0.5 ) * halfStep;

        for( int j = 0 ; j < width ; ++j )
        {
            halfJ = j*0.5;

            ypos = offsetY + j;
            upos = offsetUV + halfJ + size;
            vpos = offsetUV + halfJ + quSize;

            v11 = bufTmp[ ypos ]/255.0;
            v22 = bufferBmp[ ypos ]/255.0;

            resY = 255 * ( v11 + v22*( 1-v11 ) );

            frameBuf[ ypos ] = resY;
            frameBuf[ upos ] = bufTmp [ upos ] ;
            frameBuf[ vpos ] = bufTmp [ vpos ] ;
        }
    }
}

void frameForWritingSSE( int width, int height, unsigned char* bufTmp, unsigned char* bufferBmp, unsigned char* frameBuf )
{
    int step = width;
    int size = width * height;
    int quSize = size*1.25;
    int halfStep = step*0.5;
    int offsetY, offsetUV;

    double v11, v22;
    int halfJ;
    int ypos;
    int upos;
    int vpos;

    int resY;

    for( int i = 0 ; i < height; ++i )
    {
        offsetY = i * step;
        offsetUV = int( i * 0.5 ) * halfStep;

        for( int j = 0 ; j < width ; ++j )
        {
            halfJ = j*0.5;

            ypos = offsetY + j;
            upos = offsetUV + halfJ + size;
            vpos = offsetUV + halfJ + quSize;

            v11 = bufTmp[ ypos ]/255.0;
            v22 = bufferBmp[ ypos ]/255.0;

            resY = 255 * ( v11 + v22*( 1-v11 ) );

            frameBuf[ ypos ] = resY;
            frameBuf[ upos ] = bufTmp [ upos ] ;
            frameBuf[ vpos ] = bufTmp [ vpos ] ;
        }
    }
}

//!наложение картинки на поток по Y каналу
unsigned char* getResYChanel( unsigned char* buffer, unsigned char* bmpData, int width, int height )
{
    long frame_size_y = getImageSize( width, height );
    long frame_size_uv = getSizeUV( width, height );
    long frame_size = getYUVFrameSize( frame_size_y, frame_size_uv );

    unsigned char* yuvRes = NULL;
    try
    {
        yuvRes = new unsigned char[ frame_size ];
    }
    catch( std::exception& )
    {
        std::cout << "Allocation buffer memory error!" << std::endl;
        return NULL;
    }

    frameForWriting( width, height, buffer, bmpData, yuvRes );
    unsigned char* rgb = yuv2rgb( width, height, yuvRes );

    delete[] yuvRes;

    return rgb;
}

unsigned char* getVideoData( long& length, int& width, int& height )
{
    unsigned char* bufferVideo;
    width = 176;
    height = 144;

    std::ifstream is;
    is.open ( TEST_VIDEO, std::ios::binary );
    if( !is.is_open() )
    {
        fprintf( stderr, "Cannot open video stream" );
        return NULL;
    }
    is.seekg (0, std::ios::end);
    length = is.tellg();
    is.seekg( 0, std::ios::beg );
    try
    {
        bufferVideo = new unsigned char[length + 1];
    }
    catch( std::exception& )
    {
        fprintf( stderr, "Cannot allocate video memory" );
        return NULL;
    }

    is.read( (char* )bufferVideo, length );
    is.close();

    return bufferVideo;
}


void threadWriteY(int left, int right, unsigned char *bufferBmp, unsigned char* bufferVideo, long frame_size, int width, int height, unsigned char *bufferVideoOut )
{
    unsigned char* frameBuf = NULL;
    unsigned char* bufTmp = NULL;
    try
    {
        frameBuf = new unsigned char[ frame_size ];
        bufTmp = new unsigned char[ frame_size ];
    }
    catch( std::exception& )
    {
        if( frameBuf )
            delete[] frameBuf;
        if( bufTmp )
            delete[] bufTmp;

        std::cout << "Allocation buffer memory error!" << std::endl;
        return;
    }

    for( int i = left; i < right; ++i )
    {
        int offset = frame_size * i;
        memcpy( bufTmp, bufferVideo + offset, frame_size*sizeof(unsigned char) );
        frameForWriting( width, height, bufTmp, bufferBmp, frameBuf );

        memcpy( bufferVideoOut+offset, frameBuf, frame_size*sizeof(unsigned char) );
    }

    delete[] frameBuf;
    delete[] bufTmp;
}


void threadWriteRGB(int left, int right, unsigned char *bufferBmp, unsigned char* bufferVideo, long frame_size, int width, int height, unsigned char *bufferVideoOut )
{
    unsigned char* frameBuf = NULL;
    unsigned char* bufTmp = NULL;
    unsigned char* tmpBufRGB = NULL;
    unsigned char* bufRGBTmp = NULL;
    int size = width*height*3;

    try
    {
        frameBuf = new unsigned char[ frame_size ];
        bufTmp = new unsigned char[ frame_size ];
        bufRGBTmp = new unsigned char[ size ];
        tmpBufRGB = new unsigned char[ size ];
    }
    catch( std::exception& )
    {
        if( frameBuf )
            delete[] frameBuf;
        if( bufTmp )
            delete[] bufTmp;
        if( bufRGBTmp )
            delete[] bufRGBTmp;
        if( tmpBufRGB )
            delete[] tmpBufRGB;

        std::cout << "Allocation buffer memory error!" << std::endl;
        return;
    }

    for( int i = left; i < right; ++i )
    {
        int offset = frame_size * i;

        memcpy( bufTmp, bufferVideo + offset, frame_size*sizeof(unsigned char) );
        yuv2rgb( width, height, bufTmp, bufRGBTmp );
        getRes( bufRGBTmp, bufferBmp, tmpBufRGB, width, height );
        rgb2yuv( width, height, tmpBufRGB, frameBuf );

        memcpy( bufferVideoOut+offset, frameBuf, frame_size*sizeof(unsigned char) );
    }
}
